@echo off

net session >nul 2>&1

cd %~dp0
rundll32.exe cmroute.dll,SetRoutes /STATIC_FILE_NAME disable.txt /DONT_REQUIRE_URL /IPHLPAPI_ACCESS_DENIED_OK
