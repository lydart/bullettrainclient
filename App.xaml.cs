﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using BTC.Model;

namespace BTC
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        Mutex mutex;

        protected override void OnStartup(StartupEventArgs e)
        {
            var aIsNewInstance = false;
            mutex = new Mutex(true, "MyWPFApplication", out aIsNewInstance);
            if (!aIsNewInstance)
            {
                MessageBox.Show("新快线VPN正在运行");
                App.Current.Shutdown();
            }
            else
            {
                base.OnStartup(e);

                // Set the Application.Current.ShutdownMode to OnExplicitShutdown 
                // so that the application does not shut down after login window is closed.
                Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;

                var user = UserModel.Load();
                var loginWindow = new LoginWindow(new UserViewModel(user));
                var result = loginWindow.ShowDialog();
                if (result != null && result.Value)
                {
                    // Reset the Mainwindow and ShutdownMode properties
                    Application.Current.MainWindow = null;
                    Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

                    // Show main window. WPF will now consider this window as MainWindow of application
                    var mainWindow = new MainWindow(new ClientViewModel(user));
                    mainWindow.Show();
                }
                else
                {
                    // In case login failed or user cancels the login, then shut down application 
                    Application.Current.Shutdown();
                }
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                Common.DisableSmartRoute();

                foreach (Process proc in Process.GetProcessesByName("openvpn"))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
