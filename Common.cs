﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace BTC
{
    public class Info
    {
        public string Message { get; set; }
        public string Link { get; set; }    
    }

    public static class Common
    {
        public static string ConfigFolderPath 
        {
            get
            {
                return Path.Combine(Environment.CurrentDirectory, "configs");
            }
        }

        public static string LogFolderPath
        {
            get
            {
                return Path.Combine(Environment.CurrentDirectory, "logs");
            }
        }

        public static string UserSettingsPath
        {
            get { return Path.Combine(Environment.CurrentDirectory, "settings", "user"); }
        }

        private static string webUrl = string.Empty;
        public static string WebUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(webUrl))
                    return webUrl;

                webUrl = ServerManager.GetInstance().GetServerAddr();
                if(!string.IsNullOrEmpty(webUrl))
                    return webUrl;

                if (File.Exists(Path.Combine(Environment.CurrentDirectory, "settings", "server")))
                    webUrl = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "settings", "server")).Trim();

                if(string.IsNullOrEmpty(webUrl))
                    webUrl = "http://118.193.211.120:3000";

                return webUrl;
            }
        }

        private static string webHost = string.Empty;
        public static string WebHost
        {
            get
            {
                if (!string.IsNullOrEmpty(webHost))
                    return webHost;

                webHost = ServerManager.GetInstance().GetServerHost();
                if (!string.IsNullOrEmpty(webHost))
                    return webHost;

                if (File.Exists(Path.Combine(Environment.CurrentDirectory, "settings", "host")))
                    webHost = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "settings", "host")).Trim();

                if (string.IsNullOrEmpty(webHost))
                    webHost = "https://nbtvpn.com";

                return webHost;
            }
        }
        
        public static double PingTimeAverage(string host, int echoNum)
        {
            long totalTime = 0;
            int timeout = 1500;
            Ping pingSender = new Ping();

            for (int i = 0; i < echoNum; i++)
            {
                PingReply reply = pingSender.Send(host, timeout);
                if (reply.Status == IPStatus.Success)
                {
                    totalTime += reply.RoundtripTime;
                }
                else
                {
                    totalTime += timeout;
                }
            }
            return totalTime / echoNum;
        }

        public static void EnableSmartRoute()
        {
            var prc = new Process();
            prc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            prc.StartInfo.CreateNoWindow = true;
            prc.EnableRaisingEvents = true;
            prc.StartInfo.FileName = @".\smartroute\enable.bat";
            prc.Start();
            prc.WaitForExit();
        }

        public static void DisableSmartRoute()
        {
            var prc = new Process();
            prc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            prc.StartInfo.CreateNoWindow = true;
            prc.EnableRaisingEvents = true;
            prc.StartInfo.FileName = @".\smartroute\disable.bat";
            prc.Start();
            prc.WaitForExit();
        }

        public static Cookie Cookie = null;

        public static string Hash = "";
    }
}
