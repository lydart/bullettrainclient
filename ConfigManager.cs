﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using BTC;
using BTC.Model;
using Ionic.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WpfApplication1.Annotations;

namespace BTC
{
    public class MetaData
    {
        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("configs")]
        public List<ConfigModel> Configs { get; set; }

        public MetaData()
        {
            Version = 0;
            Configs = new List<ConfigModel>();
        }
    }

    public class ConfigsManager
    {
        private static ConfigsManager mgr;
        private static object locker = new object();
        public static ConfigsManager GetInstance()
        {
            if (mgr == null)
            {
                lock (locker)
                {
                    if (mgr == null)
                        mgr = new ConfigsManager();
                }
            }

            return mgr;
        }

        private MetaData serverMetaData;
        private string localMetaFile = Path.Combine(Common.ConfigFolderPath, "meta.json");
        private MetaData localMetaData = new MetaData();

        public List<ConfigModel> LoadConfigs()
        {
            if (NeedUpdate())
            {
                ServerManager.GetInstance().GetEnableSmartRoute();
                ServerManager.GetInstance().GetDisableSmartRoute();

                var zipPath = Download();
                Unzip(zipPath);
            }

            return ReadConfigsFromLocalMetaFile();
        }

        private bool NeedUpdate()
        {
            try
            {
                if (File.Exists(localMetaFile))
                    localMetaData = JsonConvert.DeserializeObject<MetaData>(File.ReadAllText(localMetaFile));

                var serverMgr = ServerManager.GetInstance();
                serverMetaData = serverMgr.GetRemoteMetaData();
                
                return serverMetaData.Version > localMetaData.Version;
            }
            catch (Exception)
            {
                return false;
            }
            
        }
        private string Download()
        {
            try
            {
                var serverMgr = ServerManager.GetInstance();
                return serverMgr.GetRemoteConfigs();
            }
            catch (Exception)
            {
                throw new Exception("下载配置文件失败");
            }
        }

        private void Unzip(string zipPath)
        {
            using (var zip = ZipFile.Read(zipPath))
            {
                zip.ExtractAll(Common.ConfigFolderPath, ExtractExistingFileAction.OverwriteSilently);
            }          
        }

        private List<ConfigModel> ReadConfigsFromLocalMetaFile()
        {
            if (File.Exists(localMetaFile))
                localMetaData = JsonConvert.DeserializeObject<MetaData>(File.ReadAllText(localMetaFile));

            return localMetaData.Configs;
        }
    }
}
