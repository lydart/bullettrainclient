﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using BTC.Properties;
using WpfApplication1.Annotations;
using System.Threading.Tasks;

namespace BTC.Model
{
    public class ConfigViewModel : INotifyPropertyChanged
    {
        private bool isSelected;
        public bool IsSelected
        {
            get 
            { 
                return isSelected; 
            }

            set
            {
                isSelected = value; 
                OnPropertyChanged("IsSelected");
                OnPropertyChanged("StatusImage");
            }
        }
        
        public string Title
        {
            get { return config.Title; }
            set { config.Title = value; OnPropertyChanged("Title"); }
        }
        
        public string Icon
        {
            get { return config.Icon; }
            set { config.Icon = value; OnPropertyChanged("Icon"); }
        }

        private string speedIcon;
        public string SpeedIcon
        {
            get { return speedIcon; }
            set { speedIcon = value; OnPropertyChanged("SpeedIcon"); }
        }

        public string StatusImage
        {
            get
            {
                if (IsSelected)
                    return "https://www.iconfinder.com/data/icons/fugue/icon/status.png";
                else
                    return "https://www.iconfinder.com/data/icons/fugue/icon/status-offline.png";
            }
        }

        private ConfigModel config;

        public ConfigModel Config
        {
            get { return config; }
        }

        public ConfigViewModel(ConfigModel model)
        {
            this.config = model;
            this.speedIcon = "https://cdn0.iconfinder.com/data/icons/duesseldorf/16/process.png";
        }

        public void TestSpeed()
        {
            var task = new Task(() => {
                var speed = Common.PingTimeAverage(config.Ip, 3);

                if (speed < 100)
                    SpeedIcon = "https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/128/Wifi_d6.png";
                else if (speed < 200)
                    SpeedIcon = "https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/128/Wifi_d5.png";
                else if (speed < 400)
                    SpeedIcon = "https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/128/Wifi_d4.png";
                else if (speed < 800)
                    SpeedIcon = "https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/128/Wifi_d3.png";
                else if (speed < 1000)
                    SpeedIcon = "https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/128/Wifi_d2.png";
                else
                    SpeedIcon = "https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/128/Wifi_d1.png";
            });

            task.Start();               
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
