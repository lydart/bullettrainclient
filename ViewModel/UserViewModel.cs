﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using BTC.Command;
using Newtonsoft.Json.Linq;
using WpfApplication1.Annotations;
using MessageBox = System.Windows.MessageBox;

namespace BTC.Model
{
    public class UserViewModel : INotifyPropertyChanged
    {
        private UserModel user;
        
        public string UserName
        {
            get { return user.UserName; }
            set { user.UserName = value; OnPropertyChanged("UserName");}
        }
        
        public string Password
        {
            get { return user.Password; }
            set { user.Password = value; OnPropertyChanged("Password"); }
        }
        
        public bool RememberMe
        {
            get { return user.RememberMe; }
            set { user.RememberMe = value; OnPropertyChanged("RememberMe"); }
        }

        private bool running;
        public bool Running
        {
            get { return running; }
            set
            {
                running = value;
                OnPropertyChanged("Running");
            }
        }

        private bool dialogResult;
        public bool DialogResult
        {
            get { return dialogResult; }
            set { dialogResult = value; OnPropertyChanged("DialogResult"); }
        }

        private ICommand loginCommand;
        public ICommand LoginCommand => loginCommand ?? (loginCommand = new LoginCommand(this));

        private ICommand redirectCommand;
        public ICommand RedirectCommand => redirectCommand ?? (redirectCommand = new RedirectCommand());

        public UserViewModel(UserModel user)
        {
            this.user = user;
        }

        public void Login()
        {
            if (Running)
                return;

            Running = true;
            var success = false;

            var task = new Task(() =>
            {
                string errMsg;

                var serverMgr = ServerManager.GetInstance();
                if (!serverMgr.Login(user, out errMsg))
                {
                    MessageBox.Show(errMsg);
                    return;
                }

                if (File.Exists(Common.UserSettingsPath))
                    File.Delete(Common.UserSettingsPath);

                if (user.RememberMe)
                {
                    var json = new JObject();
                    json["UserName"] = user.UserName;
                    json["Password"] = EncryptManager.Encrypt(user.Password);
                    json["Rememberme"] = user.RememberMe;

                    if (!Directory.Exists(Path.GetDirectoryName(Common.UserSettingsPath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(Common.UserSettingsPath));

                    File.WriteAllText(Common.UserSettingsPath, json.ToString());
                }
                
                var configsMgr = ConfigsManager.GetInstance();
                user.Configs = configsMgr.LoadConfigs();

                user.Advertisement = serverMgr.GetRemoteAdvertisement();
                success = true;
            });

            task.Start();
            task.ContinueWith(task1 =>
            {
                Running = false;

                if (success)
                    OnSuccessLogin?.Invoke(null, new EventArgs());
            });
        }

        public event EventHandler OnSuccessLogin;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
