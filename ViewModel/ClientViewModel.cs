﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using BTC.Command;
using WpfApplication1;
using WpfApplication1.Annotations;

namespace BTC.Model
{
    public class ClientViewModel : INotifyPropertyChanged
    {
        private string state;
        public string State
        {
            get { return state; }
            set
            {
                state = value;
                OnPropertyChanged("State");
            }
        }

        private string speed;
        public string Speed
        {
            get { return speed; }
            set
            {
                speed = value;
                OnPropertyChanged("Speed");
            }
        }

        public string error;
        public string Error
        {
            get
            {
                return error;
            }

            set
            {
                error = value;
                OnPropertyChanged("Error");
            }
        }

        public string UserStatus
        {
            get
            {
                if (user.Balance >= 1)
                    return "有效期 - " + user.Balance + "天";
                else if (user.Balance > 0)
                    return "有效期不足1天";
                else
                    return "已过期，请充值";
            }
        }

        public string UserStatusColor
        {
            get
            {
                if (user.Balance > 2)
                    return "Gray";

                return "Red";
            }
        }

        public string Advertisement
        {
            get { return user.Advertisement; }
        }
        
        public bool SmartRoute { get; set; }

        private ICommand connectCommand;
        public ICommand ConnectCommand => connectCommand ?? (connectCommand = new ConnectCommand(this));

        private ICommand disConnectedCommand;
        public ICommand DisconnectCommand => disConnectedCommand ?? (disConnectedCommand = new DisconnectCommand(this));

        private ICommand generateLogCommand;
        public ICommand GenerateLogCommand => generateLogCommand ?? (generateLogCommand = new GenerateLogCommand(this));

        private ICommand redirectCommand;
        public ICommand RedirectCommand => redirectCommand ?? (redirectCommand = new RedirectCommand());
        
        public BindingList<ConfigViewModel> Configs { get; set; }
 
        public ClientViewModel(UserModel user)
        {
            Configs = new BindingList<ConfigViewModel>();
            Configs.ListChanged += (sender, args) => OnPropertyChanged(string.Empty);

            this.user = user;

            foreach (var config in user.Configs)
            {
                var configVM = new ConfigViewModel(config);
                configVM.TestSpeed();
                Configs.Add(configVM);
            }

            
            State = "未连接VPN";
            Speed = "下载：0B/s 上传： 0B/s";
            SmartRoute = true;
            errors = new List<string>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private UserModel user;

        private VPNManager vpnMgr;
        private CancellationTokenSource source;       
        private Int64 downloaded;
        private Int64 uploaded;
        private bool connected;
        private List<string> errors; 

        public bool Connected
        {
            get
            {
                return this.connected;
            }
        }

        public void Connect()
        {
            if (source != null && !source.IsCancellationRequested)
            {
                MessageBox.Show("请先点击 “断开连接” 按钮，断开当前连接");
                return;
            }

            var selectedConfig = Configs.FirstOrDefault(x => x.IsSelected);
            if (selectedConfig == null)
            {
                MessageBox.Show("请先选择专线");
                return;
            }

            var configFile = Path.Combine(Path.GetTempPath(), selectedConfig.Config.File);

            File.Copy(Path.Combine(Common.ConfigFolderPath, selectedConfig.Config.File), configFile, true);
            

            if (!File.Exists(configFile))
            {
                MessageBox.Show("找不到配置文件");
                return;
            }

            downloaded = 0;
            uploaded = 0;
            connected = false;

            source = new CancellationTokenSource();
            State = "正在建立VPN连接...";
            

            if (selectedConfig.Config.Type.Equals("public", StringComparison.InvariantCultureIgnoreCase))
            {
                vpnMgr = new VPNManager("localhost", 9632, configFile, "vpn", "vpn", "openvpn.exe", SmartRoute, selectedConfig.Config);
            }
            else
            {
                vpnMgr = new VPNManager("localhost", 9632, configFile, user.UserName, user.Password, "openvpn.exe", SmartRoute, selectedConfig.Config);
            }
            

            vpnMgr.SetEchoOn();
            vpnMgr.SetStateOn();
            vpnMgr.GetByteCount(1);

            var factory = new TaskFactory();

            var monitorTask = factory.StartNew(() =>
            {
                while (true)
                {
                    if (source.Token.IsCancellationRequested)
                    {
                        connected = false;
                        source.Token.ThrowIfCancellationRequested();
                    }

                    try
                    {
                        var messages = vpnMgr.ReceiveNotification();
                        foreach (var message in messages)
                        {
                            if (message.Contains(">STATE"))
                            {
                                if (message.Contains("GET_CONFIG"))
                                    State = "正在配置VPN...";
                                else if (message.Contains("ASSIGN_IP"))
                                    State = "正在分配IP地址...";
                                else if (message.Contains("CONNECTED"))
                                {
                                    State = "连接成功 - " + selectedConfig.Title;

                                    if (!connected)
                                    {
                                        connected = ServerManager.GetInstance().SetMark();
                                        if (!connected && source != null && !source.IsCancellationRequested)
                                        {
                                            MessageBox.Show("连接服务器出错,请重新启动程序后再试");
                                            source.Cancel();
                                            vpnMgr.SendSignal(VPNManager.Signal.Term);
                                        }
                                    }
                                }
                                else if (message.Contains("EXITING"))
                                    State = "连接断开!";
                            }
                            else if (message.Contains(">BYTECOUNT"))
                            {
                                var data = message.Replace(">BYTECOUNT:", "");
                                var dataArray = data.Split(',');

                                if (dataArray.Length == 2)
                                {
                                    Speed = string.Format("下载：{0}/s 上传： {1}/s", GetSpeed(Int64.Parse(dataArray[0]) - downloaded), GetSpeed(Int64.Parse(dataArray[1]) - uploaded));

                                    downloaded = Int64.Parse(dataArray[0]);
                                    uploaded = Int64.Parse(dataArray[1]);
                                }
                            }
                            else if (message.Contains("ERROR:"))
                            {
                                errors.Add(message);
                                Error = "检查到错误，查看日志";
                            }
                            else if (message.Contains("CONNECTED,SUCCESS"))
                            {
                                State = "连接成功 - " + selectedConfig.Title;
                                if (!connected)
                                {
                                    connected = ServerManager.GetInstance().SetMark();
                                    if (!connected && source != null && !source.IsCancellationRequested)
                                    {
                                        MessageBox.Show("连接服务器出错,请重新启动程序后再试");
                                        source.Cancel();
                                        vpnMgr.SendSignal(VPNManager.Signal.Term);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        State = "连接失败";
                        Speed = "下载：0B/s 上传： 0B/s";
                        errors.Add(ex.Message);
                        Error = "检查到错误，查看日志";

                        if (source != null && !source.IsCancellationRequested)
                        {
                            source.Cancel();
                            vpnMgr.SendSignal(VPNManager.Signal.Term);
                        }
                    }
                }
            }, source.Token);

            var stateWatchDogTask = factory.StartNew(() =>
            {
                while (!connected)
                {
                    Thread.Sleep(1000);
                    vpnMgr.GetState();

                    if (source.Token.IsCancellationRequested)
                    {
                        connected = false;
                        source.Token.ThrowIfCancellationRequested();                    
                    }
                }
            }, source.Token);

            var connWatchDogTask = factory.StartNew(() =>
            {
                while (true)
                {
                    Thread.Sleep(1000*60*3);

                    if(!connected)
                        continue;

                    var errMsg = string.Empty;
                    var valid = ServerManager.GetInstance().CheckMark(ref errMsg);
                    if (!valid && source != null && !source.IsCancellationRequested)
                    {
                        source.Cancel();
                        vpnMgr.SendSignal(VPNManager.Signal.Term);
                        State = errMsg;
                        MessageBox.Show(errMsg);
                    }

                    if (source.Token.IsCancellationRequested)
                    {
                        connected = false;
                        source.Token.ThrowIfCancellationRequested();                       
                    }       
                }
            }, source.Token);

            factory.ContinueWhenAll(new Task[] {monitorTask, stateWatchDogTask, connWatchDogTask }, delegate
            {
                State = "未连接VPN";
                Speed = "下载：0B/s 上传： 0B/s";
            });
        }

        public void Disconnect()
        {
            if (source != null && !source.IsCancellationRequested)
            {
                source.Cancel();
                vpnMgr.SendSignal(VPNManager.Signal.Term);
            }

            State = "未连接VPN";
            Speed = "下载：0B/s 上传： 0B/s";
        }

        public void GenerateLog()
        {
            var err = new List<string>();

            try
            {   
                err.AddRange(errors);
            }
            catch (Exception)
            {
            }

            if (!Directory.Exists(Common.LogFolderPath))
                Directory.CreateDirectory(Common.LogFolderPath);

            var log = Path.Combine(Common.LogFolderPath, DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss") + ".log");
            File.AppendAllLines(log, err);
            
            System.Diagnostics.Process.Start(log);
        }

        private string GetSpeed(Int64 value)
        {
            var speed = ((double)(value));
            if (speed < 1e3)
                return (speed).ToString("F") + "B";

            if (speed < 1e6)
                return (speed / 1e3).ToString("F") + "KB";

            if (speed < 1e9)
                return (speed / 1e6).ToString("F") + "MB";

            return (speed / 1e9).ToString("F") + "GB";
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
