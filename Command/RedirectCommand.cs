﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BTC.Model;

namespace BTC.Command
{
    class RedirectCommand : ICommand
    {
        public RedirectCommand()
        {
        }

        public void Execute(object parameter)
        {
            System.Diagnostics.Process.Start(Common.WebHost);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
