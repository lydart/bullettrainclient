﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BTC.Model;

namespace BTC.Command
{
    class LoginCommand : ICommand
    {
        private readonly UserViewModel model;

        public LoginCommand(UserViewModel model)
        {
            this.model = model;
        }

        public void Execute(object parameter)
        {
            this.model.Login();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
