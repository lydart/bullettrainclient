﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BTC.Model;
using System.Windows;

namespace BTC.Command
{
    public class DisconnectCommand : ICommand
    {
        private readonly ClientViewModel model;

        public DisconnectCommand(ClientViewModel model)
        {
            this.model = model;
        }

        public void Execute(object parameter)
        {
            if (this.model.Connected && MessageBox.Show("确定要断开当前连接？", "断开连接", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return;

            this.model.Disconnect();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
