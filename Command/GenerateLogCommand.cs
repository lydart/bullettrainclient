﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BTC.Model;

namespace BTC.Command
{
    public class GenerateLogCommand : ICommand
    {
        private readonly ClientViewModel model;

        public GenerateLogCommand(ClientViewModel model)
        {
            this.model = model;
        }

        public void Execute(object parameter)
        {
            this.model.GenerateLog();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
