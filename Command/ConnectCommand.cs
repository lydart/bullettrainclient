﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using BTC.Model;

namespace BTC.Command
{
    public class ConnectCommand : ICommand
    {
        private readonly ClientViewModel model;

        public ConnectCommand(ClientViewModel model)
        {
            this.model = model;
        }

        public void Execute(object parameter)
        {
            this.model.Connect();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}
