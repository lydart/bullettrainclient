﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.IO;
using System.Net.NetworkInformation;
using BTC.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BTC
{
    public class ServerManager
    {
        private static ServerManager mgr;
        private static object locker = new object();

        public static ServerManager GetInstance()
        {
            if (mgr == null)
            {
                lock (locker)
                {
                    if (mgr == null)
                        mgr = new ServerManager();
                }
            }

            return mgr;
        }

        public bool Login(UserModel user, out string errorMsg)
        {
            errorMsg = string.Empty;

            CookieContainer cookies = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;

            using (var client = new HttpClient(handler))
            {
                var values = new Dictionary<string, string>
                {
                    {"username", user.UserName},
                    {"password", user.Password}
                };

                var content = new FormUrlEncodedContent(values);

                HttpResponseMessage response;

                try
                {
                    response = client.PostAsync(Common.WebUrl + "/api/users/login", content).Result;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    errorMsg = "登录超时";
                    return false;
                }

                if (!response.IsSuccessStatusCode)
                {
                    errorMsg = "用户名或密码错误";
                    return false;
                }

                IEnumerable<Cookie> responseCookies =
                    cookies.GetCookies(new Uri(Common.WebUrl + "/api/users/login")).Cast<Cookie>();
                foreach (Cookie cookie in responseCookies)
                     Common.Cookie = cookie;

                var resContent = response.Content.ReadAsStringAsync().Result;

                var resJson = JObject.Parse(resContent);

                if (resJson["status"].ToString() != "success")
                {
                    errorMsg = resJson["errMessage"].ToString();
                    return false;
                }

                user.Balance = Convert.ToDouble(resJson["balance"].ToString());
                return true;
            }

        }

        public MetaData GetRemoteMetaData()
        {
            using (var client = new WebClient())
            {
                var metaFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".json");

                client.Headers.Add(HttpRequestHeader.Cookie, Common.Cookie.Name + "=" + Common.Cookie.Value);
                client.DownloadFile(Common.WebUrl + "/api/download/meta", metaFile);
                return JsonConvert.DeserializeObject<MetaData>(File.ReadAllText(metaFile));
            }
        }

        public string GetRemoteConfigs()
        {
            using (var client = new WebClient())
            {
                var configsZip = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".zip");

                client.Headers.Add(HttpRequestHeader.Cookie, Common.Cookie.Name + "=" + Common.Cookie.Value);
                client.DownloadFile(Common.WebUrl + "/api/download/configs", configsZip);

                return configsZip;
            }
        }

        public string GetRemoteAdvertisement()
        {
            using (var client = new WebClient())
            {
                var adFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".rtf");

                client.DownloadFile(Common.WebUrl + "/api/download/ad", adFile);

                return adFile;
            }
        }

        public bool SetMark()
        {
            using (WebClient client = new WebClient())
            {
                Common.Hash = Guid.NewGuid().ToString();
                string param = "device=PC&hash=" + Common.Hash;
                client.Headers.Add(HttpRequestHeader.Cookie, Common.Cookie.Name + "=" + Common.Cookie.Value);
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    var result = client.UploadString(Common.WebUrl + "/api/users/setmark", param);
                    var resJson = JObject.Parse(result);

                    return resJson["status"].ToString() == "success";
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    return false;
                }
                
            }
        }

        public bool CheckMark(ref string errMsg)
        {
            using (WebClient client = new WebClient())
            {
                string param = "device=PC&hash=" + Common.Hash;
                client.Headers.Add(HttpRequestHeader.Cookie, Common.Cookie.Name + "=" + Common.Cookie.Value);
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    var result = client.UploadString(Common.WebUrl + "/api/users/getmark", param);
                    var resJson = JObject.Parse(result);

                    if (resJson["errCode"].ToString() == "0")
                        errMsg = "当前账号可能出现异地登录，连接即将断开";
                    else
                        errMsg = "当前账号余额不足，请充值。 连接即将断开";

                    return resJson["status"].ToString() == "success";
                }
                catch (Exception)
                {
                    errMsg = "服务器连接异常";
                    return false;
                }

            }
        }

        public Info GetInfo()
        {
            var ret = new Info();

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    var result = client.DownloadString("https://coding.net/u/lydart/p/nbtvpn/git/raw/master/info-2.0.json");
                    var resJson = JObject.Parse(result);

                    ret.Message = resJson["message"].ToString();
                    ret.Link = resJson["link"].ToString();
                }
                catch (Exception)
                {
                    // ignored
                }

                return ret;
            }
        }

        public string GetServerAddr()
        {
            string ret = string.Empty;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    ret = client.DownloadString("https://coding.net/u/lydart/p/nbtvpn/git/raw/master/server");

                }
                catch (Exception)
                {
                    // ignored
                }                
            }

            return ret;
        }

        public string GetServerHost()
        {
            string ret = string.Empty;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    ret = client.DownloadString("https://coding.net/u/lydart/p/nbtvpn/git/raw/master/host");

                }
                catch (Exception)
                {
                    // ignored
                }
            }

            return ret;
        }

        public string GetEnableSmartRoute()
        {
            string ret = string.Empty;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    ret = client.DownloadString("https://coding.net/u/lydart/p/nbtvpn/git/raw/master/enable");
                    if(!string.IsNullOrEmpty(ret))
                        File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "smartroute", "enable.txt"), ret);
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            return ret;
        }

        public string GetDisableSmartRoute()
        {
            string ret = string.Empty;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                try
                {
                    ret = client.DownloadString("https://coding.net/u/lydart/p/nbtvpn/git/raw/master/disable");
                    if (!string.IsNullOrEmpty(ret))
                        File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "smartroute", "disable.txt"), ret);
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            return ret;
        }
    }
}
