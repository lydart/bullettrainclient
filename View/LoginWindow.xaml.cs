﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using BTC;
using BTC.Model;
using BTC.View;
using MahApps.Metro.Controls;

namespace BTC
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        private string message;
        private string link;

        public LoginWindow(UserViewModel model)
        {
            InitializeComponent();

            DataContext = model;

            model.OnSuccessLogin += (sender, args) =>
            {
                Action action = delegate ()
                {
                    this.DialogResult = true;
                };

                Dispatcher.Invoke(action);
            };

            var task = new Task(() =>
            {
                var info = ServerManager.GetInstance().GetInfo();
                message = info.Message;
                link = info.Link;
            });

            task.Start();
            task.ContinueWith(ui => { MessageText.Text = message; }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            var window = new MessageWindow();

            window.ShowDialog();
        }

        private void InfoHyperlink_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(link))
                System.Diagnostics.Process.Start(link);
        }
        
    }
}
