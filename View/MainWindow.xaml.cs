﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BTC;
using BTC.Model;
using MahApps.Metro.Controls;

namespace BTC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow(ClientViewModel model)
        {
            InitializeComponent();

            DataContext = model;
        }

        private void CheckBoxSmartRoute_Click(object sender, RoutedEventArgs e)
        {
            if (((ClientViewModel)DataContext).Connected)
            {
                System.Windows.MessageBox.Show("更改智能路由配置前, 请先断开当前连接", "智能路由", MessageBoxButton.OK, MessageBoxImage.Information);
                CheckBoxSmartRoute.IsChecked = !CheckBoxSmartRoute.IsChecked;
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show("确定要退出新快线VPN？", "退出新快线VPN", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                e.Cancel = true;
        }
    }
}
