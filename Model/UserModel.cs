﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace BTC.Model
{
    public class UserModel
    {
        public string UserName { get; set; }
        
        public string Password { get; set; }
        
        public bool RememberMe { get; set; }

        public double Balance { get; set; }

        public string Advertisement { get; set; }

        public IList<ConfigModel> Configs { get; set; } 

        private UserModel()
        {
        }

        public static UserModel Load()
        {
            try
            {
                if (File.Exists(Common.UserSettingsPath))
                {
                    var user = JsonConvert.DeserializeObject<UserModel>(File.ReadAllText(Common.UserSettingsPath));
                    user.Password = EncryptManager.Decrypt(user.Password);
                    return user;
                }

                return new UserModel();
            }
            catch (Exception)
            {
                return new UserModel();
            }
        }
    }
}
