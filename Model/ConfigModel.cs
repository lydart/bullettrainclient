﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTC.Model
{
    public class ConfigModel
    {
        public string Title { get; set; }

        public string Icon { get; set; }

        public string File { get; set; }

        public string Ip { get; set; }

        public string Port { get; set; }

        public string Protol { get; set; }

        public string Type { get; set; }
    }
}
